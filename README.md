ENV SETTINGS
=====

Manage environment specific configuration in a YAML file and quickly apply to a target environment with a singe command.  The emvsettings commands are made available as magento cli commands. 

See Unit test directory for a sample YAML file.

Commands
---

DUMP:

Dumps all config stored in core_confid data in YAML format for the specified scope. 

    php bin/magento envsettings:dump store 1 ./

DIFF:

Diff the settings defined for an environment in a YAML file with the settings defined within Magento.  

    php bin/magento envsettings:diff development ./site-config-store-1-1551459487.yml


APPLY:

Set and save all the settings defined for an environment in a YAML file in a Magento instance.  Supports a dry run flag.  

    php bin/magento envsettings:apply development ./site-config-store-1-1551459487.yml [-d]

Additional Options:
* --silent Suppress output of config changes
* --applydiff only apply settings change if it differs between the settings file and the DB

File Structure
----

Environment > scope-$ID > setting > value

The config path is built from the indented keys 

    development:
        store-1:
            design:
                pagination:
                    list_allow_all: '1'
                    pagination_frame: '5'
                    pagination_frame_skip: null
        
        store-2:
                design:
                    pagination:
                        list_allow_all: '2'
                        pagination_frame: '5'
                        pagination_frame_skip: null
    
    production:
            store-1:
                design:
                    pagination:
                        list_allow_all: '1'
                        pagination_frame: '2'
                        pagination_frame_skip: null



Encrypted values
----

Sensitive passwords that are stored as encrypted values in magento can be stored in these files as either encrypted or decrypted, and then prefixed with either 'encrypt::' or 'decrypt::' so the envsettings module knows what to do with them.

For example

    public_key: 'decrypt::0asd:sda-accasd-sda1233' - value will be decrypted before diffing, but applyed as is.
    
    private key: 'encrypt::0asd:sda-accasd-sda1233' - value will not be decrypted for diffingm but will be encrypted when applyed.  

Reference:

* 
