<?php
namespace Magento\CommandExample\Test\Unit\Console\Command;

use Symfony\Component\Console\Tester\CommandTester;
use TheDevelopmentSuite\Envsettings\Console\Command\ApplyCommand;

class ApplyCommandTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Settings for testing
     */
    const TEST_CONFIG_FILE_NAME = 'unit-test-settings-1.yaml';

    /**
     * @var CheckActiveModulesCommand
     */
    private $command;

    /**
     * @var \Magento\Framework\Module\ModuleListInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $moduleList;

    /**
     * Standard setup suitable for most test cases
     */
    public function setUp()
    {
        $mockConfigWriter = $this->createMock(\Magento\Framework\App\Config\Storage\Writer::class);
        $mockConfigWriter->expects($this->any())->method('save')->willReturn('');

        $this->command = new ApplyCommand($mockConfigWriter);
    }


    /**
     * Test sample production scope is extracted from the config file and applied
     *
     */
    public function testApplyCommand_Prod()
    {
        $commandTester = new CommandTester($this->command);

        $commandTester->execute(
            [
                ApplyCommand::ENV_ARGUMENT => 'Production',
                ApplyCommand::ENV_FILE_ARGUMENT=> dirname(__FILE__) . '/' . SELF::TEST_CONFIG_FILE_NAME
            ]
        );

        $this->assertContains('Setting Applied. KEY: config/dev/test VALUE: 0 SCOPE: store SCOPE ID: 1', $commandTester->getDisplay());
        $this->assertContains('Setting Applied. KEY: config/dev/test2 VALUE: text config prod SCOPE: store SCOPE ID: 1', $commandTester->getDisplay());
        $this->assertNotContains('Setting Applied. KEY: config/dev/test2 VALUE: text config website 1 SCOPE: website', $commandTester->getDisplay());
    }


    /**
     * Test sample development scope is extracted from the config file and applied
     *
     */
    public function testApplyCommand_Development()
    {
        $commandTester = new CommandTester($this->command);

        $commandTester->execute(
            [
                ApplyCommand::ENV_ARGUMENT => 'Development',
                ApplyCommand::ENV_FILE_ARGUMENT=> dirname(__FILE__) . '/' . SELF::TEST_CONFIG_FILE_NAME
            ]
        );

        $this->assertContains('Setting Applied. KEY: config/dev/test VALUE: 1 SCOPE: default SCOPE ID: 0', $commandTester->getDisplay());
        $this->assertContains('Setting Applied. KEY: config/dev/test2 VALUE: text config development store -1 SCOPE: default SCOPE ID: 0', $commandTester->getDisplay());
        $this->assertNotContains('Setting Applied. KEY: config/dev/test2 VALUE: text config website 1 SCOPE: website', $commandTester->getDisplay());
    }


    /**
     * Test sample staging scope is extracted from the config file and applied
     *
     */
    public function testApplyCommand_Staging()
    {
        $commandTester = new CommandTester($this->command);

        $commandTester->execute(
            [
                ApplyCommand::ENV_ARGUMENT => 'Staging',
                ApplyCommand::ENV_FILE_ARGUMENT=> dirname(__FILE__) . '/' . SELF::TEST_CONFIG_FILE_NAME
            ]
        );

        $commandTester->getDisplay();
        $this->assertContains('Setting Applied. KEY: config/dev/test VALUE: 1 SCOPE: default SCOPE ID: 0', $commandTester->getDisplay());
        $this->assertContains('Setting Applied. KEY: config/dev/test VALUE: 3 SCOPE: website SCOPE ID: 3', $commandTester->getDisplay());
        $this->assertContains('Setting Applied. KEY: config/dev/test2 VALUE: text config staging store 1 SCOPE: default SCOPE ID: 0', $commandTester->getDisplay());
        $this->assertContains('Setting Applied. KEY: config/dev/test2 VALUE: text config website 3 SCOPE: website SCOPE ID: 3', $commandTester->getDisplay());
        $this->assertNotContains('Setting Applied. KEY: config/dev/test2 VALUE: text config development store -1', $commandTester->getDisplay());
    }


    /**
     * Test dry run flag does not save and config
     *
     */
    public function testApplyCommand_DryRunFlag()
    {
        $mockConfigWriter = $this->createMock(\Magento\Framework\App\Config\Storage\Writer::class);
        $mockConfigWriter->expects($this->never())->method('save');

        $this->command = new ApplyCommand($mockConfigWriter);

        $commandTester = new CommandTester($this->command);

        $commandTester->execute(
            [
                ApplyCommand::ENV_ARGUMENT => 'Staging',
                ApplyCommand::ENV_FILE_ARGUMENT=> dirname(__FILE__) . '/' . SELF::TEST_CONFIG_FILE_NAME,
                '-d' => true
            ]
        );

        $commandTester->getDisplay();

    }

}
