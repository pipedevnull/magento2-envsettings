<?php
/**
 *
 */

namespace TheDevelopmentSuite\Envsettings\Console\Command;

use Magento\Store\Model\ScopeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Env Settings
 *
 * Class ApplyCommand
 */
class DiffCommand extends Command
{
    /**
     * Name argument
     */
    const ENV_ARGUMENT = 'environment';

    /**
     * ENV file argument
     */
    const ENV_FILE_ARGUMENT = 'environment_file';

    /**
     * scope type argument
     */
    const SCOPE_TYPE_ARGUMENT = 'scope-type';

    /**
     * scope ID argument
     */
    const SCOPE_ID_ARGUMENT = 'scope-id';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $configData;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encrypter;


    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $configData
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $configData,
                                \Magento\Framework\Encryption\EncryptorInterface $encrypter)
    {
        $this->configData = $configData;
        $this->encrypter    = $encrypter;

        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('envsettings:diff')
            ->setDescription('Diff settings command')
            ->setDefinition([
                new InputArgument(
                    self::ENV_ARGUMENT,
                    InputArgument::REQUIRED,
                    'Environment name'
                ),
                new InputArgument(
                    self::ENV_FILE_ARGUMENT,
                    InputArgument::REQUIRED,
                    'Environment settings file'
                )
            ]);

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Required Parameters
        $env         = $input->getArgument(self::ENV_ARGUMENT);
        $envFilePath = $input->getArgument(self::ENV_FILE_ARGUMENT);
        //$scopeId     = $input->getArgument(self::SCOPE_ID_ARGUMENTx);
        //$scopeType   = $input->getArgument(self::SCOPE_TYPE_ARGUMENT);

        if (!file_exists($envFilePath)) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_FILE_ARGUMENT . ' path is invalid.');
        }

        // Load and parse YAML file
        // Settings Array Structure: [EnvName => [ scope-id => [config-$id : [config-key => config-value,config-key => config-value ... ]... ]...]]
        $data = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($envFilePath));

        // Check the target environment name exists in the environment settings file
        if (!array_key_exists($env, $data)) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_ARGUMENT. ' is not a valid environment name');
        }

        // Loop through settings in the target ENV and compare them
        foreach ($data[$env] as $scope => $settings) {

            //Decode the scope section header
            $scope = $this->decodeScopeSectionHeader($scope);

            $this->diff($settings, $scope, $output);
        }
    }


    /**
     * Perform recursive diff on settings
     *
     * @param $settings
     * @param $scope
     * @param OutputInterface $output
     */
    protected function diff($settings, $scope,  OutputInterface $output, $parentKey = array())
    {
        // Apply the settings in the right scope.
        foreach ($settings as $key => $value) {

            if (is_array($value)){
                $parentKey[] = $key;
                $this->diff($value, $scope, $output, $parentKey);
                array_pop($parentKey);
            } else {

                $path       = implode('/', $parentKey) . '/' . $key;

                $value = $this->handleEncryption($value, $output);

                $mageValue  = $this->configData->getValue($path, $scope['scope'], $scope['ID']);

                if ($mageValue != $value) {
                    $output->writeln("<error>Setting diff. KEY: $path SCOPE: ${scope['scope']} SCOPE ID: ${scope['ID']} YAML VALUE: $value Magento Value: $mageValue</error>");
                }
            }
        }
    }


    /**
     * Check value for any encryption markers and encrypt / decrypt accordingly
     *
     * @param $value
     * @param $output
     * @return string
     * @throws \Exception
     */
    protected function handleEncryption($value, $output)
    {
        if (preg_match('/^encrypt::.*/', $value)) {

            $valueParts = explode('encrypt::', $value);

            if (isset($valueParts[1])) {
                return $valueParts[1];
            } else {
                $output->writeln("<error>Setting Cannot be applied. Encrypt string format incorrect for $value</error>");
                throw new \Exception("Setting Cannot be applied. Encrypt string format incorrect for $value");
            }
        } else if (preg_match('/^decrypt::.*/', $value)) {

            $valueParts = explode('decrypt::', $value);

            if (isset($valueParts[1])) {
                return $this->encrypter->decrypt(($valueParts[1]));
            } else {
                $output->writeln("<error>Setting Cannot be applied. Encrypt string format incorrect for $value</error>");
                throw new \Exception("Setting Cannot be applied. Encrypt string format incorrect for $value");
            }
        }

        return $value;
    }



    /**
     * Split out the scope config section header into an array
     *
     * @param string $scope
     * @return array[]
     *
     * @throws \ReflectionException|\InvalidArgumentException
     */
    protected function decodeScopeSectionHeader(string $scope) : array
    {
        $scopeParts = preg_split('/-/', $scope);

        if (!count($scopeParts) ==2) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_FILE_ARGUMENT. ': Incorrect format of config section - ' . $scope);
        }

        $scopeClass             = new \ReflectionClass(ScopeInterface::class);

        $validScopes            = $scopeClass->getConstants();
        $validScopes['DEFAULT'] = 'default';

        if (!in_array($scopeParts[0], $validScopes)) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_FILE_ARGUMENT. ': Contains invalid scope definition - ' . $scopeParts[0]);
        }

        return array(
            "scope" => $scopeParts[0],
            "ID"    => $scopeParts[1]
        );
    }

}
