<?php
/**
 *
 */

namespace TheDevelopmentSuite\Envsettings\Console\Command;

use Magento\Store\Model\ScopeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Env Settings
 *
 * Class ApplyCommand
 */
class ApplyCommand extends Command
{
    /**
     * Name argument
     */
    const ENV_ARGUMENT = 'environment';

    /**
     * ENV file argument
     */
    const ENV_FILE_ARGUMENT = 'environment_file';

    /**
     * Flag to only apply config differences
     */
    const APPLY_DIFF = 'applydiff';

    /**
     * Flag to suppress output
     */
    const SILENCE = 'silence';

    /**
     * @var Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $configData;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encrypter;

    /**
     * Dry run option
     */
    const DRY_RUN = 'dry-run';


    /**
     * @param ModuleListInterface $moduleList
     */
    public function __construct(\Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
                                \Magento\Framework\App\Config\ScopeConfigInterface $configData,
                                \Magento\Framework\Encryption\EncryptorInterface $encrypter)
    {
        $this->configWriter = $configWriter;
        $this->encrypter    = $encrypter;
        $this->configData   = $configData;

        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('envsettings:apply')
            ->setDescription('Apply settings command')
            ->setDefinition([
                new InputArgument(
                    self::ENV_ARGUMENT,
                    InputArgument::REQUIRED,
                    'Environment name'
                ),
                new InputArgument(
                    self::ENV_FILE_ARGUMENT,
                    InputArgument::REQUIRED,
                    'Environment settings file'
                ),
                new InputOption(
                    self::DRY_RUN,
                    '-d',
                    InputOption::VALUE_OPTIONAL,
                    'Dry run'
                ),
                new InputOption(
                    self::SILENCE,
                    '-s',
                    InputOption::VALUE_NONE,
                    'Silent mode'
                ),
                new InputOption(
                    self::APPLY_DIFF,
                    '-i',
                    InputOption::VALUE_NONE,
                    'Only apply config if different between settings file and DB'
                ),

            ]);

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Required Parameters
        $env         = $input->getArgument(self::ENV_ARGUMENT);
        $envFilePath = $input->getArgument(self::ENV_FILE_ARGUMENT);
        $applyDiff   = $input->getOption(self::APPLY_DIFF);
        $silent      = $input->getOption(self::SILENCE);
        $dryRun      = $input->getOption(self::DRY_RUN);

        if (!file_exists($envFilePath)) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_FILE_ARGUMENT . ' path is invalid.');
        }

        // Load and parse YAML file
        // Settings Array Structure: [EnvName => [ scope-id => [config-$id : [config-key => config-value,config-key => config-value ... ]... ]...]]
        $data = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($envFilePath));

        // Check the target environment name exists in the environment settings file
        if (!array_key_exists($env, $data)) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_ARGUMENT. ' is not a valid environment name');
        }

        // Loop through settings and apply them
        foreach ($data[$env] as $scope => $settings) {

            //Decode the scope section header
            $scope = $this->decodeScopeSectionHeader($scope);

            // Apply the settings in the right scope.
            $this->apply($settings, $scope, $output, array(), $dryRun, $applyDiff, $silent);
        }
        $output->writeln('<info>Settings Applied! Remember to flush the cache</info>');
    }


    /**
     * Apply settings - recurse through YAML to identify the correct config paths
     *
     * @param $settings
     * @param $scope
     * @param OutputInterface $output
     * @param array $parentKey
     * @param $dryRun
     * @throws \Exception
     */
    protected function apply($settings, $scope,  OutputInterface $output, $parentKey = array(), $dryRun, $applyDiff, $silent)
    {
        // Apply the settings in the right scope.
        foreach ($settings as $key => $value) {

            if (is_array($value)){
                $parentKey[] = $key;
                $this->apply($value, $scope, $output, $parentKey, $dryRun, $applyDiff, $silent);
                array_pop($parentKey);
            } else {

                $path = implode('/', $parentKey) . '/' . $key;

                $value = $this->handleEncryption($value, $output);

                if ($applyDiff) {

                    $mageValue = $this->configData->getValue($path, $scope['scope'], $scope['ID']);

                    if ($mageValue == $value) {
                        continue 1;
                    }
                }

                if (!$dryRun) {
                    $this->configWriter->save($path, $value, $scope['scope'], $scope['ID']);
                }

                if (!$silent){
                    $output->writeln("<info>Setting Applied. KEY: $key VALUE: $value SCOPE: ${scope['scope']} SCOPE ID: ${scope['ID']}</info>");
                }
            }
        }
    }


    /**
     * Check value for any encryption markers and encrypt / decrypt accordingly
     *
     * @param $value
     * @param $output
     * @return string
     * @throws \Exception
     */
    protected function handleEncryption($value, $output)
    {
        if (preg_match('/^encrypt::.*/', $value)) {

            $valueParts = explode('encrypt::', $value);

            if (isset($valueParts[1])) {
                return $this->encrypter->encrypt(($valueParts[1]));
            } else {
                $output->writeln("<error>Setting Cannot be applied. Encrypt string format incorrect for $value</error>");
                throw new \Exception("Setting Cannot be applied. Encrypt string format incorrect for $value");
            }
        } else if (preg_match('/^decrypt::.*/', $value)) {

            $valueParts = explode('decrypt::', $value);

            if (isset($valueParts[1])) {
                return $valueParts[1];
            } else {
                $output->writeln("<error>Setting Cannot be applied. Encrypt string format incorrect for $value</error>");
                throw new \Exception("Setting Cannot be applied. Encrypt string format incorrect for $value");
            }
        }

        return $value;
    }


    /**
     * Split out the scope config section header into an array
     *
     * @param string $scope
     * @return array[]
     *
     * @throws \ReflectionException|\InvalidArgumentException
     */
    protected function decodeScopeSectionHeader(string $scope) : array
    {
        $scopeParts = preg_split('/-/', $scope);

        if (!count($scopeParts) ==2) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_FILE_ARGUMENT. ': Incorrect format of config section - ' . $scope);
        }

        $scopeClass             = new \ReflectionClass(ScopeInterface::class);

        $validScopes            = $scopeClass->getConstants();
        $validScopes['DEFAULT'] = 'default';

        if (!in_array($scopeParts[0], $validScopes)) {

            throw new \InvalidArgumentException('Argument ' . self::ENV_FILE_ARGUMENT. ': Contains invalid scope definition - ' . $scopeParts[0]);
        }

        return array(
            "scope" => $scopeParts[0],
            "ID"    => $scopeParts[1]
        );
    }

}
