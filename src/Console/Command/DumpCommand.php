<?php

namespace TheDevelopmentSuite\Envsettings\Console\Command;

use Magento\Store\Model\ScopeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Yaml;

/**
 * Class DumpCommand
 */
class DumpCommand extends Command
{
    /**
     * scope type argument
     */
    const SCOPE_TYPE_ARGUMENT = 'scope-type';

    /**
     * scope ID argument
     */
    const SCOPE_ID_ARGUMENT = 'scope-id';

    /**
     * Output location argument
     */
    const OUTPUT_ARGUMENT = 'output-directory-path';

    /**
     *
     */
    const INDENT_SPACES = 4;

    /**
     * @var Magento\Framework\App\Config\Storage\Data
     */
    protected $configData;


    /**
     * DumpCommand constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $configData
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $configData)
    {
        $this->configData = $configData;
        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('envsettings:dump')
            ->setDescription('Dump core config settings in YAML format for ENV')
            ->setDefinition([
                new InputArgument(
                    self::SCOPE_TYPE_ARGUMENT,
                    InputArgument::REQUIRED,
                    'scope type'
                ),
                new InputArgument(
                    self::SCOPE_ID_ARGUMENT,
                    InputArgument::REQUIRED,
                    'scope ID'
                ),
                new InputArgument(
                    self::OUTPUT_ARGUMENT,
                    InputArgument::REQUIRED,
                    'YAML file output diretory'
                ),
            ]);

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // Required Parameters
        $scopeId         = $input->getArgument(self::SCOPE_ID_ARGUMENT);
        $scopeType       = $input->getArgument(self::SCOPE_TYPE_ARGUMENT);
        $outputDir       = $input->getArgument(self::OUTPUT_ARGUMENT);

        if (!is_dir($outputDir) || !is_writable($outputDir))
        {
            throw new \InvalidArgumentException('Argument ' . self::OUTPUT_ARGUMENT. ' is not a valid directory');
        }

        $data = $this->configData->getValue(null, $scopeType, $scopeId);

        $dumper     = new Dumper();
        $yaml = $dumper->dump($data, 4);

        $outputFile = $outputDir . "/site-config-$scopeType-$scopeId-" . time() . '.yml';

        file_put_contents($outputFile, $yaml);

        $output->writeLn("<info>Config exported to $outputFile</info>");
    }


}
